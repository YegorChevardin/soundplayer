# SoundPlayer
Plays sound that was mentioned in course project (7 variant)

## How to use?
- Install [JDK](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html) >= **8** version;
- Run `.jar` file with following command: `java -jar SoundPlayer.jar`;
- Or if you do not want to run it as a `.jar` file you can always clone this repository and open it in Java IDE such as [Eclipse](https://www.eclipse.org/) or [intellij IDEA](https://www.jetbrains.com/idea/old/)

# 
>╭━━━╮╱╱╱╱╱╱╱╱╱╱╭┳━━━┳╮<br/>
>┃╭━╮┃╱╱╱╱╱╱╱╱╱╱┃┃╭━╮┃┃<br/>
>┃╰━━┳━━┳╮╭┳━╮╭━╯┃╰━╯┃┃╭━━┳╮╱╭┳━━┳━╮<br/>
>╰━━╮┃╭╮┃┃┃┃╭╮┫╭╮┃╭━━┫┃┃╭╮┃┃╱┃┃┃━┫╭╯<br/>
>┃╰━╯┃╰╯┃╰╯┃┃┃┃╰╯┃┃╱╱┃╰┫╭╮┃╰━╯┃┃━┫┃<br/>
>╰━━━┻━━┻━━┻╯╰┻━━┻╯╱╱╰━┻╯╰┻━╮╭┻━━┻╯<br/>
>╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╭━╯┃<br/>
>╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╱╰━━╯