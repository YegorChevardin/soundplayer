package com.kryzha.soundplayer.utils;

import javax.sound.sampled.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class SoundPLayer {
    private static class AudioListener implements LineListener {
        private boolean done = false;
        @Override public synchronized void update(LineEvent event) {
            LineEvent.Type eventType = event.getType();
            if (eventType == LineEvent.Type.STOP || eventType == LineEvent.Type.CLOSE) {
                done = true;
                notifyAll();
            }
        }
        public synchronized void waitUntilDone() throws InterruptedException {
            while (!done) { wait(); }
        }
    }

    public void start(String fileToPlay) throws UnsupportedAudioFileException, LineUnavailableException, IOException, InterruptedException {
        AudioListener listener = new AudioListener();
        try (InputStream inputStream = new BufferedInputStream(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(fileToPlay)));
             AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(inputStream)) {
            Clip clip = AudioSystem.getClip();
            clip.addLineListener(listener);
            clip.open(audioInputStream);
            try {
                clip.start();
                listener.waitUntilDone();
            } finally {
                clip.close();
            }
        }
    }
}
