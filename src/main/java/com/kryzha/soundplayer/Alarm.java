package com.kryzha.soundplayer;

import com.kryzha.soundplayer.utils.SoundPLayer;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.net.URISyntaxException;

public class Alarm {
    private static Alarm instance = null;
    private static final int SECONDS = 10;
    private static final String FA = "fa.wav";
    private static final String RE = "re.wav";
    private static final String CI = "ci.wav";

    private Alarm() {}

    public static Alarm getInstance() {
        if (instance == null) {
            instance = new Alarm();
        }
        return instance;
    }

    public void start() {
        printLogo();
        System.out.println("Wait for 10 seconds!");
        System.out.println("********************");
        try {
            System.out.println("Program is started!");
            Thread.sleep(SECONDS * 1000);
            playSound(FA, RE, CI);
            System.out.println("Sound is played!");
        } catch (Exception e) {
            System.out.println("Some error occurred: " + e.getMessage());
            e.printStackTrace();
        }
        System.out.println("********************");
        System.out.println("Program is finished!");
    }

    private void printLogo() {
        System.out.println(
                "╭━━━╮          ╭┳━━━┳╮\n" +
                "┃╭━╮┃          ┃┃╭━╮┃┃\n" +
                "┃╰━━┳━━┳╮╭┳━╮╭━╯┃╰━╯┃┃╭━━┳╮ ╭┳━━┳━╮\n" +
                "╰━━╮┃╭╮┃┃┃┃╭╮┫╭╮┃╭━━┫┃┃╭╮┃┃ ┃┃┃━┫╭╯\n" +
                "┃╰━╯┃╰╯┃╰╯┃┃┃┃╰╯┃┃  ┃╰┫╭╮┃╰━╯┃┃━┫┃\n" +
                "╰━━━┻━━┻━━┻╯╰┻━━┻╯  ╰━┻╯╰┻━╮╭┻━━┻╯\n" +
                "                         ╭━╯┃\n" +
                "                         ╰━━╯");
    }

    private void playSound(String... filesToPlay) throws UnsupportedAudioFileException, LineUnavailableException, IOException, URISyntaxException, InterruptedException {
        SoundPLayer soundPLayer = new SoundPLayer();
        for (String file : filesToPlay) {
            soundPLayer.start(file);
        }
    }
}
